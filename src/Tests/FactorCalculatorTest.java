/*package Tests;

import org.junit.Assert;
import static org.junit.Assert.fail;
import org.junit.Test;

public class FactorCalculatorTest //extends TestCase
{
    @Test 
    public void F1_factor()
    {
        int number=100;
        int[] resultado;
        int[] esperado={2,2,5,5};
        FactorCalculator fact = new FactorCalculator();
        
        resultado=fact.factor(100);
        
        Assert.assertArrayEquals(esperado, resultado);
    }
    
    @Test
    public void F2_factor()
    {
        int number=4;
        int[] resultado;
        int[] esperado={2,2};
        FactorCalculator fact = new FactorCalculator();
        
        resultado=fact.factor(number);
        Assert.assertArrayEquals(resultado, esperado);
    }
    
    @Test
    public void F3_factor()
    {
        int number=3;
        int[] resultado;
        int[] esperado={3};
        FactorCalculator fact = new FactorCalculator();
        
        resultado=fact.factor(number);
        Assert.assertArrayEquals(resultado, esperado); 
    }
    
    @Test
    public void F4_factor()
    {
        int number=2;
        int[] resultado;
        int[] esperado={2};
        FactorCalculator fact = new FactorCalculator();
        
        resultado=fact.factor(number);
        Assert.assertArrayEquals(resultado, esperado); 
    }
    
    @Test
    public void F5_factor()
    {
        int number=2;
        FactorCalculator fact = new FactorCalculator();
        
        Assert.assertTrue(fact.isPrime(number));
    }
    
    @Test
    public void F6_factor()
    {
        int number=3;
        FactorCalculator fact = new FactorCalculator();
        
        Assert.assertTrue(fact.isPrime(number));
    }
    
    @Test
    public void F7_factor()
    {
        int number=4;
        FactorCalculator fact = new FactorCalculator();
        
        Assert.assertFalse(fact.isPrime(number));
    }
    
    @Test
    public void F8_factor()
    {
        int number=1;
        FactorCalculator fact = new FactorCalculator();
       
        try
        {
            Assert.assertTrue(fact.isPrime(number));
            fail("Error: el valor de entrada es ilegal");
        }catch(IllegalArgumentException e)
        {
            e.getMessage();
        }
    }
    
    @Test
    public void F9_factor()
    {
        int compositeNumber=6;
        int potentialDivisor=3;
        FactorCalculator fact = new FactorCalculator();
        
        Assert.assertTrue(fact.isDivisor(compositeNumber, potentialDivisor));
    }
    
    @Test
    public void F10_factor()
    {
        int compositeNumber=5;
        int potentialDivisor=2;
        FactorCalculator fact = new FactorCalculator();
        
        Assert.assertFalse(fact.isDivisor(compositeNumber, potentialDivisor));
    }
    
    @Test
    public void F11_factor()
    {
        int compositeNumber=6;
        int potentialDivisor=0;
        FactorCalculator fact = new FactorCalculator();
        
        Assert.assertTrue(fact.isDivisor(compositeNumber, potentialDivisor));
        
    }
}

*/